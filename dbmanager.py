from sqlalchemy import *
from sqlalchemy.orm import declarative_base, relationship, Session

########################################################################################
#Establish database connections

engine = create_engine("sqlite+pysqlite:///Block5Ex1DB.db", echo=False, future=True)

connection = engine.connect()
session = Session(engine)
metadata = MetaData()
t_products = Table('Products', metadata, autoload=True, autoload_with=engine)
t_customers = Table('Customers', metadata, autoload=True, autoload_with=engine)
t_receipts = Table('Receipts', metadata, autoload=True, autoload_with=engine)

########################################################################################
#Establish controllers

class Controller(): #(No longer used)
    '''
    Class for controlling this program as a user
    '''
    
def purchase(username:str, product:str):
    '''
    Adds a receipt of purchase to the receipts table
    '''
    session.execute(insert(t_receipts).values((username.capitalize(), product)))
    session.commit()

def view_receipts(username:str):
    '''
    Checks the receipts of a given user
    '''
    results = session.execute(select(t_receipts).where(Column('Cust_Name') == username.capitalize()))
    for item in results.all():
        print(item[1])

class Admin(Controller): #9No longer used)
    '''
    Class for utilizing this program with admin priviledges
    '''
def add_customer():
        '''
        Add customer to the Customers table
        '''
        name = input('Name: ')
        phone = input('Phone: ')
        id = int(input('ID: '))
        session.execute(insert(t_customers).values((id, name.capitalize(), phone)))
        session.commit()
    
def add_product():
        '''
        Add product to the product table
        '''
        description = input('Description: ')
        price = input('Price: ')
        id = input('ID: ')

        session.execute(insert(t_products).values((id, description.title(), price)))
        session.commit()

def remove_customer():
        '''
        Remove a customer from the Customers table
        '''
        key = input("Which Customer would you like to remove?\n")
        session.execute(delete(t_customers).where(Column('Name') == key.capitalize()))
        session.commit()

def remove_product():
        '''
        Remove a product from the products table
        '''
        product = input("Which product would you like to remove?\n")
        session.execute(delete(t_products).where(Column('Description') == product.title()))
        session.commit()
        
def view_all_receipts():
        result = session.query(t_receipts)
        for item in result.all():
            print(f'{item[0]} bought {item[1]}.')